An exploratory project on the role of depth in the UNet architecture's ability
to retain information. I use the Microsoft COCO Stuff semantic segmenatation
dataset. Please see paper.pdf and poster.pdf for more details.

Final project for Stanford CS231N: Convolutional Neural Networks
for Visual Recognition.
