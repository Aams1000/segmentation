import torch
from models.modifiedResnet import ModifiedResnet


class CLArgs(object):

    class Runmode:
        TRAIN = "--train"
        TEST = "--test"
        TOY = "--toy"
        TINY = "--tiny"
        TEST_ONE_IMAGE = "--testOneImage"
        CALCULATE_MEANS = "--calculateMean"
        VALIDATE = "--validate"

    class Device:
        CUDA = "cuda"
        CPU = "cpu"

    class ResnetFlag:
        FULL_101 = "--resnet101"

    class ModelType:
        BASELINE = "--baseline"
        UNET = "--unet"
        RESNET_UNET = "--resnetUnet"


    def __init__(self, args):
        self.argsMap = args
        self.CUDA_FLAG = "--cuda"
        self.TEST_FLAG = "--test"
        self.FLAG_RESUME = "--resume"
        self.FORCE_LR_FLAG = "--forceLr"
        self.BASELINE_FLAG = "--baseline"

    def getDevice(self) -> torch.device:
        deviceString = None
        if self.argsMap[self.CUDA_FLAG]:
            deviceString = self.Device.CUDA
        else:
            deviceString = self.Device.CPU
        return torch.device(deviceString)

    def getRunmode(self) -> str:
        if self.argsMap[self.Runmode.TRAIN]:
            return self.Runmode.TRAIN
        elif self.argsMap[self.Runmode.TEST]:
            return self.Runmode.TEST
        elif self.argsMap[self.Runmode.TOY]:
            return self.Runmode.TOY
        elif self.argsMap[self.Runmode.TINY]:
            return self.Runmode.TINY
        elif self.argsMap[self.Runmode.TEST_ONE_IMAGE]:
            return self.Runmode.TEST_ONE_IMAGE
        elif self.argsMap[self.Runmode.CALCULATE_MEANS]:
            return self.Runmode.CALCULATE_MEANS
        elif self.argsMap[self.Runmode.VALIDATE]:
            return self.Runmode.VALIDATE
        else:
            raise RuntimeError("Invalid runmode. Please include --train, --test, or --toy")

    def getResnetType(self) -> int:
        if self.argsMap[self.ResnetFlag.FULL_101]:
            return ModifiedResnet.ResnetType.FULL_101
        else:
            return ModifiedResnet.ResnetType.SMALL_34

    def isTest(self) -> bool:
        return self.argsMap[self.TEST_FLAG]

    def isResumeRun(self) -> bool:
        return self.argsMap[self.FLAG_RESUME]

    def shouldForceNewLearningRate(self) -> bool:
        return self.argsMap[self.FORCE_LR_FLAG]

    def getModelType(self) -> str:
        if self.argsMap[self.ModelType.BASELINE]:
            return self.ModelType.BASELINE
        elif self.argsMap[self.ModelType.UNET]:
            return self.ModelType.UNET
        elif self.argsMap[self.ModelType.RESNET_UNET]:
            return self.ModelType.RESNET_UNET
        else:
            raise RuntimeError("Invalid model type. Please include --unet, --resnetUnet, or --baseline")

