import torch
from customlogging import Logging
import torch.nn.functional as F

class TensorOperations:

    @staticmethod
    def concatenateXAndOther(x: torch.Tensor, outputToConcat: torch.Tensor, shouldExpandX: bool = False) -> torch.Tensor:
        # shavedTensor = outputToConcat[:, :, :x.shape[2], :x.shape[3]]

        # Logging.log("X: " + str(x.size()))
        # Logging.log("Candidate: " + str(outputToConcat.size()))
        # Logging.log("Shaved: " + str(shavedTensor.size()))
        # Logging.log("Beginning concatenation")
        # due to padding, this can sometimes happen
        if outputToConcat.shape[3] < x.shape[3]:
            difference = x.shape[3] - outputToConcat.shape[3]
        #     outputToConcat = F.pad(input=outputToConcat, pad=[difference // 2, difference - (difference // 2)], mode='constant', value=0)
            outputToConcat = F.pad(input=outputToConcat, pad=[0, difference], mode='constant', value=0)
        if shouldExpandX:
            hDifference = max(outputToConcat.shape[2] - x.shape[2], 0)
            wDifference = max(outputToConcat.shape[3] - x.shape[3], 0)
            if hDifference > 0 or wDifference > 0:
                x = F.pad(input=x, pad=[0, wDifference, 0, hDifference], mode='constant', value=0)

        # Logging.log("Updated X: " + str(x.size()))
        # Logging.log("Updated Candidate: " + str(outputToConcat.size()))
        if shouldExpandX:
            x = torch.cat([x, outputToConcat[:, :, :max(x.shape[2], outputToConcat.shape[2]), :max(x.shape[3], outputToConcat.shape[3])]], dim=1)

        else:
            x = torch.cat([x, outputToConcat[:, :, :x.shape[2], :x.shape[3]]], dim=1)
        # Logging.log("Concatenated: " + str(x.size()))
        return x

