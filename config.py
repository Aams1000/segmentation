class Config:

    # Network/module params
    class NN:
        BATCH_SIZE = 32
        MAX_EPOCHS = 300
        # LEARNING_RATE = 5e-3
        # LEARNING_RATE_UNET = 8e-5
        # LEARNING_RATE_UNET = 1e-4 too high?
        # LEARNING_RATE_UNET = 5e-5
        # LEARNING_RATE_UNET = 2e-5
        # LEARNING_RATE_UNET = 5e-6
        # LEARNING_RATE_UNET = 4e-5
        # LEARNING_RATE_UNET = 1.5e-5
        # LEARNING_RATE_UNET = 7.5e-6
        LEARNING_RATE_UNET = 9e-7

        LEARNING_RATE_BASELINE = 8e-4
        # LEARNING_RATE_RESNET_UNET = 5e-4 * 0.1 too low
        # LEARNING_RATE_RESNET_UNET = 2.5e-4 * 0.2 too high?
        # LEARNING_RATE_RESNET_UNET = 2.5e-4 * 0.15 too high?
        # LEARNING_RATE_RESNET_UNET = 2.5e-4 * 0.125 too high?
        # LEARNING_RATE_RESNET_UNET = 2.5e-4 * 0.11 too high?
        # LEARNING_RATE_RESNET_UNET = 2.5e-4 * 0.04
        # LEARNING_RATE_RESNET_UNET = 5e-6
        # LEARNING_RATE_RESNET_UNET = 1e-6
        # LEARNING_RATE_RESNET_UNET = 5e-7
        # LEARNING_RATE_RESNET_UNET = 5e-5
        # LEARNING_RATE_RESNET_UNET = 1e-5
        # LEARNING_RATE_RESNET_UNET = 4e-5
        # LEARNING_RATE_RESNET_UNET = 2e-5
        # LEARNING_RATE_RESNET_UNET = 7.5e-6
        LEARNING_RATE_RESNET_UNET = 2.5e-6

    # LEARNING_RATE = 1e-6
        # LEARNING_RATE = 2e-4

        REGULARIZATION = 1e-3
        # REGULARIZATION = 0
        # ANNEAL_FREQUENCY = 250
        # ANNEAL_FREQUENCY = 1000
        # ANNEAL_FREQUENCY_RESNET_UNET = {350: 0.5}
        ANNEAL_FREQUENCY_RESNET_UNET = {}
        ANNEAL_FREQUENCY_UNET = {}
        ANNEAL_FREQUENCY_BASELINE = {}

        ANNEAL_PERCENTAGE = 0.85

        LOG_FREQUENCY = 10
        SAVE_FREQUENCY = 1000

        SHOW_FREQUENCY = 15
        VALIDATION_FREQUENCY = 200
        # VALIDATION_FREQUENCY = 50

        # this makes the resnet's output the final conv layer before the average pooling
        NUM_RESNET_LAYERS_TO_TRUNCATE = 2

        TESTING_BATCH_SIZE = 1

        NUM_ITERATIONS_FOR_AVERAGE = 10

    #File/data config
    class IO:
        NUM_DATALOADER_THREADS = 3
