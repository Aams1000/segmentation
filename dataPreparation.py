import torch
import torch.nn as nn
from fileio.datasets import Dataset


class DataPreparation:

    @staticmethod
    def standardizeAndPrepareInputs(inputs: torch.Tensor, modifiedResnet: nn.Module):
        inputs = Dataset.standardizeInputs(inputs)
        while len(inputs.shape) < 4:
            inputs = inputs.unsqueeze(0)
        inputs = modifiedResnet(inputs)
        return inputs

