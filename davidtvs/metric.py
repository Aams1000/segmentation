# Note: everything in the davidtvs package is from https://github.com/davidtvs/PyTorch-ENet. I use this publicly
# available implementation of the IoU loss, as PyTorch does not currently include a native implementation.


class Metric(object):
    """Base class for all metrics.
    From: https://github.com/pytorch/tnt/blob/master/torchnet/meter/meter.py
    """
    def reset(self):
        pass

    def add(self):
        pass

    def value(self):
        pass