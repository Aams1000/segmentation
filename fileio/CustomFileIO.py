from typing import *
import torch
from PIL import Image, ImageOps
import numpy as np
import os
from customlogging import Logging


class CustomFileIO(object):

    class DataType:
        TINY = "tiny_toy/"
        TOY = "toy/"
        TRAIN = "train/"
        VAL = "val/"
        TEST = "test/"

    class ImageType:
        INPUT = "inputs/"
        LABEL = "labels/"

    class ObjectType:
        MODEL = ".model"
        OPTIMIZER = ".optim"

    __DATA_DIR = "./data/coco/"
    __RESULTS_DIR = "./results/"
    __RESULTS_PREFIX = "bestResults"

    @staticmethod
    def getDirectoryString(dataType: str, imageType: str) -> str:
        return CustomFileIO.__DATA_DIR + dataType + imageType


    @staticmethod
    def getIdsFromFolder(dataType: str, imageType: str) -> List[str]:
        directoryString = CustomFileIO.getDirectoryString(dataType, imageType)
        ids = [fileName.split(".")[0] for fileName in os.listdir(directoryString)]
        return ids

    @staticmethod
    def getIdsForDataType(dataType: str) -> Dict[int, str]:
        ids = CustomFileIO.getIdsFromFolder(dataType, CustomFileIO.ImageType.INPUT)
        idToStringId = {}
        for i, id in enumerate(ids):
            idToStringId[i] = id
        return idToStringId

    @staticmethod
    def loadInputImage(dataType: str, id: str, targetHeightAndWidth: Tuple[int, int], device: torch.device) -> torch.Tensor:
        return CustomFileIO.__loadImage(dataType, id, CustomFileIO.ImageType.INPUT, targetHeightAndWidth, device, ".jpg")

    @staticmethod
    def loadLabelImage(dataType: str, id: str, targetHeightAndWidth: Tuple[int, int], device: torch.device) -> torch.Tensor:
        rawTensor = CustomFileIO.__loadImage(dataType, id, CustomFileIO.ImageType.LABEL, targetHeightAndWidth, device, ".png")
        # COCO's Stuff classes range from 92 - 183. I also add my own padding class. To shift the labels to zero, we take the max of (0, rawTensor - 91)
        subtractedTensor = (rawTensor - 91)
        nonNegativeMask = (subtractedTensor > 0).float()
        notTooHighMask = (subtractedTensor <= 92).float()
        return (subtractedTensor * nonNegativeMask * notTooHighMask).long().to(device)

    @staticmethod
    def loadAndResizePilImage(dataType: str, id: str, imageType: str, targetHeightAndWidth: Tuple[int, int], fileSuffix: str, isInputImage: bool) -> Image.Image:
        path = CustomFileIO.getDirectoryString(dataType, imageType) + id + fileSuffix

        image = CustomFileIO.__cropOrPadImage(Image.open(path), targetHeightAndWidth)
        # some images are greyscale
        if isInputImage and len(image.size) == 2:
            rgbImage = Image.new("RGB", image.size)
            rgbImage.paste(image)
            image = rgbImage

        return image

    @staticmethod
    def __loadImage(dataType: str, id: str, imageType: str, targetHeightAndWidth: Tuple[int, int], device: torch.device, fileSuffix: str) -> torch.Tensor:
        isInputImage = CustomFileIO.ImageType.INPUT == imageType

        image = CustomFileIO.loadAndResizePilImage(dataType, id, imageType, targetHeightAndWidth, fileSuffix, isInputImage)

        npArray = np.asarray(image)
        image = torch.from_numpy(npArray).float().to(device)
        if isInputImage:
            image = image.permute((2, 0, 1))

        return image

    @staticmethod
    def __cropOrPadImage(image: Image.Image, heightAndWidth: Tuple[int, int]) -> Image.Image:
        originalSize = image.size
        targetHeight, targetWidth = heightAndWidth
        image.thumbnail(heightAndWidth)
        croppedHeight, croppedWidth = image.size
        heightDifference = targetHeight - croppedHeight
        widthDifference = targetWidth - croppedWidth
        paddingValues = None
        if widthDifference > 0 or heightDifference > 0:
            halfWidthDifferenceFloor = int(widthDifference / 2)
            halfHeightDifferenceFloor = int(heightDifference / 2)
            if widthDifference > 0 and heightDifference > 0:
                paddingValues = (halfHeightDifferenceFloor,
                                 halfWidthDifferenceFloor,
                                 heightDifference - halfHeightDifferenceFloor,
                                 widthDifference - halfWidthDifferenceFloor)
            elif widthDifference > 0:
                paddingValues = (0,
                                 halfWidthDifferenceFloor,
                                 0,
                                 widthDifference - halfWidthDifferenceFloor)
            else:
                paddingValues = (halfHeightDifferenceFloor,
                                 0,
                                 heightDifference - halfHeightDifferenceFloor,
                                 0)
        if paddingValues is not None:
            preexistingPalette = image.palette
            image = ImageOps.expand(image, paddingValues)
            if preexistingPalette is not None:
                image.putpalette(preexistingPalette.palette)

        if (targetHeight, targetWidth) != image.size:
            print("Original: " + str(originalSize))
            print("Cropped: " + str((croppedHeight, croppedWidth)))
            print("Processed: " + str(image.size))
            print(paddingValues)

        return image

    # horrible hardcoding of non-grayscale validation image
    @staticmethod
    def getLabelImagePalette():
        path = CustomFileIO.getDirectoryString(CustomFileIO.DataType.VAL, CustomFileIO.ImageType.LABEL) + "000000000802" + ".png"
        return Image.open(path).palette

    @staticmethod
    def saveModelStateDict(stateDict: Dict, runMode: str, resnetType: str, runId: str) -> None:
        CustomFileIO.__saveStateDict(stateDict, runMode, resnetType, runId, CustomFileIO.ObjectType.MODEL)

    @staticmethod
    def saveOptimizerStateDict(stateDict: Dict, runMode: str, resnetType: str, runId: str) -> None:
        CustomFileIO.__saveStateDict(stateDict, runMode, resnetType, runId, CustomFileIO.ObjectType.OPTIMIZER)

    @staticmethod
    def __saveStateDict(stateDict: Dict, runMode: str, resnetType: str, runId: str, objectTypeSuffix: str) -> None:
        path = CustomFileIO.__RESULTS_DIR + runMode[2:] + "_res" + resnetType + "_" + runId + objectTypeSuffix
        torch.save(stateDict, path)
        Logging.log("Saved state dict to " + path)

    @staticmethod
    def loadModelStateDict(fileName: str, deviceType: str = None) -> Dict:
        return CustomFileIO.__loadStateDict(fileName, CustomFileIO.ObjectType.MODEL, deviceType)

    @staticmethod
    def loadOptimizerStateDict(fileName: str, deviceType: str = None) -> Dict:
        return CustomFileIO.__loadStateDict(fileName, CustomFileIO.ObjectType.OPTIMIZER, deviceType)

    @staticmethod
    def __loadStateDict(fileName: str, objectTypeSuffix: str, deviceType: str = None) -> Dict:
        if deviceType is not None:
            return torch.load(CustomFileIO.__RESULTS_DIR + fileName + objectTypeSuffix, map_location=deviceType)
        else:
            return torch.load(CustomFileIO.__RESULTS_DIR + fileName + objectTypeSuffix)

if __name__ == "__main__":
    # fileIO = FileIO()
    print(CustomFileIO.getIdsFromFolder(CustomFileIO.DataType.VAL, CustomFileIO.ImageType.INPUT))