import torch
import torchvision
from torch.utils import data
from fileio.CustomFileIO import CustomFileIO
from clargs import CLArgs

# VAL
# Finished 5000 images. Final metrics:
# Median width: 640.0
# Median height: 480.0
# Median aspect ratio: 1.3333333333333333

# TEST
# Finished 40670 images. Final metrics:
# Median width: 640.0
# Median height: 480.0
# Median aspect ratio: 1.3333333333333333

# Classes in VAL set
# <class 'list'>: [0.0, 92.0, 93.0, 94.0, 95.0, 96.0, 97.0, 98.0, 99.0, 100.0, 101.0, 102.0, 103.0, 104.0, 105.0, 106.0, 107.0, 108.0, 109.0, 110.0, 111.0, 112.0, 113.0, 114.0, 115.0, 116.0, 117.0, 118.0, 119.0, 120.0, 121.0, 122.0, 123.0, 124.0, 125.0, 126.0, 127.0, 128.0, 129.0, 130.0, 131.0, 132.0, 133.0, 134.0, 135.0, 136.0, 137.0, 138.0, 139.0, 140.0, 141.0, 142.0, 143.0, 144.0, 145.0, 146.0, 147.0, 148.0, 149.0, 150.0, 151.0, 152.0, 153.0, 154.0, 155.0, 156.0, 157.0, 158.0, 159.0, 160.0, 161.0, 162.0, 163.0, 164.0, 165.0, 166.0, 167.0, 168.0, 169.0, 170.0, 171.0, 172.0, 173.0, 174.0, 175.0, 176.0, 177.0, 178.0, 179.0, 180.0, 181.0, 182.0, 183.0]


# (17:03:57) Finished first pass
# (17:03:57) Channel 0 sum tensor(11203402.) , mean tensor(98.8940)
# (17:03:57) Channel 1 sum tensor(10655295.) , mean tensor(94.0558)
# (17:03:57) Channel 2 sum tensor(9712355.) , mean tensor(85.7323)
# (17:03:57) Commencing epoch 1
# (17:13:30) Processed 20000 samples, current stds tensor(77.7657), tensor(75.6786), tensor(76.5635)
# (17:22:45) Processed 40000 samples, current stds tensor(77.8235), tensor(75.7335), tensor(76.6288)
# (17:32:09) Processed 60000 samples, current stds tensor(77.8713), tensor(75.7697), tensor(76.6550)
# (17:41:36) Processed 80000 samples, current stds tensor(77.8251), tensor(75.7226), tensor(76.6278)
# (17:51:20) Processed 100000 samples, current stds tensor(77.8298), tensor(75.7046), tensor(76.6000)
# (17:57:39) Finished second pass
# (17:57:39) Channel 0 std tensor(77.8005)
# (17:57:39) Channel 1 std tensor(75.6680)
# (17:57:39) Channel 2 std tensor(76.5814)

# Drawn from https://stanford.edu/~shervine/blog/pytorch-how-to-generate-data-parallel
class Dataset(data.Dataset):

    # 92 COCO Stuff classes plus one for padding
    NUM_CLASSES = 93
    IGNORE_INDEX = 0
    # RESNET_INPUT_SIZE = (640, 480)
    RESNET_INPUT_SIZE = (320, 240)

    class ChannelMeans:
        ZERO = 98.8940
        ONE = 94.0558
        TWO = 85.7323

    class ChannelStds:
        ZERO = 77.8005
        ONE = 75.6680
        TWO = 76.5814

    def __init__(self, dataType: str, device: torch.device):
        self.idsToStringMap = CustomFileIO.getIdsForDataType(dataType)

        self.device = device
        self.dataType = dataType

    def __len__(self):
        return len(self.idsToStringMap)

    def __getitem__(self, index):
        id = self.idsToStringMap[index]
        input = CustomFileIO.loadInputImage(self.dataType, id, Dataset.RESNET_INPUT_SIZE, self.device)
        label = CustomFileIO.loadLabelImage(self.dataType, id, Dataset.RESNET_INPUT_SIZE, self.device)
        return input, label

    @staticmethod
    def standardizeInputs(inputs: torch.Tensor) -> torch.Tensor:
        # Logging.log("Standardizing input")
        inputs[:, 0, :, :] = (inputs[:, 0, :, :] - Dataset.ChannelMeans.ZERO) / Dataset.ChannelStds.ZERO
        # Logging.log("Channel 0: " + str(inputs[:, 0, :, :].mean()) + ", " + str(inputs[:, 0, :, :].std()))
        inputs[:, 1, :, :] = (inputs[:, 1, :, :] - Dataset.ChannelMeans.ONE) / Dataset.ChannelStds.ONE
        # Logging.log("Channel 1: " + str(inputs[:, 1, :, :].mean()) + ", " + str(inputs[:, 1, :, :].std()))
        inputs[:, 2, :, :] = (inputs[:, 2, :, :] - Dataset.ChannelMeans.TWO) / Dataset.ChannelStds.TWO
        # Logging.log("Channel 2: " + str(inputs[:, 2, :, :].mean()) + ", " + str(inputs[:, 2, :, :].std()))
        return inputs

if __name__ == "__main__":
    dataSet = Dataset(CustomFileIO.DataType.VAL, torch.device(CLArgs.Device.CPU))
    pretrainedResnet = torchvision.models.resnet18(pretrained=True)
