from config import Config
from clargs import CLArgs
import torch
from customlogging import Logging
from typing import *


class LROperations:

    @staticmethod
    def getLearningRate(modelType: str) -> float:
        if CLArgs.ModelType.UNET == modelType:
            return Config.NN.LEARNING_RATE_UNET
        elif CLArgs.ModelType.BASELINE == modelType:
            return Config.NN.LEARNING_RATE_BASELINE
        elif CLArgs.ModelType.RESNET_UNET == modelType:
            return Config.NN.LEARNING_RATE_RESNET_UNET
        else:
            raise ValueError("Invalid model type: " + str(modelType))

    @staticmethod
    def annealLearningRateIfApplicable(optimizer: torch.optim.Optimizer, modelType: str, iteration: int) -> None:
        iterToPercentageMap = LROperations.__getAnnealSetForModelType(modelType)
        if iteration in iterToPercentageMap.keys():
            LROperations.annealLearningRate(optimizer, iterToPercentageMap[iteration])

    @staticmethod
    def __getAnnealSetForModelType(modelType: str) -> Dict[int, float]:
        if modelType == CLArgs.ModelType.RESNET_UNET:
            return Config.NN.ANNEAL_FREQUENCY_RESNET_UNET
        elif modelType == CLArgs.ModelType.UNET:
            return Config.NN.ANNEAL_FREQUENCY_UNET
        elif modelType == CLArgs.ModelType.BASELINE:
            return Config.NN.ANNEAL_FREQUENCY_BASELINE
        else:
            raise ValueError("Invalid model type: " + str(modelType))

    @staticmethod
    def annealLearningRate(optimizer: torch.optim.Optimizer, annealPercentage: float) -> None:
        if len(optimizer.param_groups) > 0 and "lr" in optimizer.param_groups[0]:
            oldRate = optimizer.param_groups[0]["lr"]
            newRate = oldRate * annealPercentage
            Logging.log("Annealing learning rate by " + str(annealPercentage) + " from " + str(oldRate) + " to " + str(newRate))
            LROperations.forceUpdateLearningRate(optimizer, newRate)
        else:
            Logging.log("Not annealing learning rate due to absense of optimizer or learning rate in optimizer.param_groups")

    @staticmethod
    def forceUpdateLearningRate(optimizer: torch.optim.Optimizer, newRate: float) -> None:
        for param_group in optimizer.param_groups:
            param_group['lr'] = newRate

