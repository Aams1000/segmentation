import torch.nn as nn
import torch
from customlogging import Logging
from models.modifiedResnet import ModifiedResnet
from models.baselineUpconvBlock import BaselineUpconvBlock
from models.lossComputations import LossComputations
from typing import *


class BaselineModel(nn.Module):
    def __init__(self, numClasses: int, ignoreIndex: int, resnetType: int, device: torch.device):
        super(BaselineModel, self).__init__()
        self.modifiedResnet = ModifiedResnet(type=resnetType).to(device)
        self.resnetType = resnetType

        firstUpconvInputChannels = ModifiedResnet.FINAL_NUM_CHANNELS_34 if resnetType == ModifiedResnet.ResnetType.SMALL_34 else ModifiedResnet.FINAL_NUM_CHANNELS_101
        firstUpconvOutputChannels = firstUpconvInputChannels // 2

        self.firstUpConv = BaselineUpconvBlock(firstUpconvInputChannels,
                                       firstUpconvOutputChannels,
                                       firstConvOutChannels=firstUpconvOutputChannels,
                                       secondConvOutChannels=firstUpconvOutputChannels,
                                       kernelSize=3,
                                       stride=2,
                                       padding=1,
                                       includeFinalBatchNorm=True).to(device)

        secondUpconvOutputChannels = firstUpconvOutputChannels // 2
        self.secondUpConv = BaselineUpconvBlock(firstUpconvOutputChannels,
                                        secondUpconvOutputChannels,
                                        firstConvOutChannels=secondUpconvOutputChannels,
                                        secondConvOutChannels=secondUpconvOutputChannels,
                                        kernelSize=3,
                                        stride=2,
                                        padding=1,
                                        includeFinalBatchNorm=True).to(device)

        thirdUpconvOutputChannels = secondUpconvOutputChannels // 2
        self.thirdUpConv = BaselineUpconvBlock(secondUpconvOutputChannels,
                                       thirdUpconvOutputChannels,
                                       firstConvOutChannels=thirdUpconvOutputChannels,
                                       secondConvOutChannels=thirdUpconvOutputChannels,
                                       kernelSize=3,
                                       stride=2,
                                       padding=0,
                                       includeFinalBatchNorm=True).to(device)

        fourthUpconvInputChannels = thirdUpconvOutputChannels
        # if resnetType == ModifiedResnet.ResnetType.FULL_101:
        #     firstDimensionAugmentationOutputChannels = thirdUpconvOutputChannels // 2
        #     self.firstDimensionAugmentationLayer = nn.Conv2d(thirdUpconvOutputChannels, firstDimensionAugmentationOutputChannels, kernel_size=1)
        #     fourthUpconvInputChannels = firstDimensionAugmentationOutputChannels
        # else:
        #     firstDimensionAugmentationOutputChannels = thirdUpconvOutputChannels * 2
        #     self.firstDimensionAugmentationLayer = nn.Conv2d(thirdUpconvOutputChannels, firstDimensionAugmentationOutputChannels, kernel_size=1)
        #     fourthUpconvInputChannels = firstDimensionAugmentationOutputChannels

        fourthUpconvOutputChannels = fourthUpconvInputChannels // 2


        #         # Logging.log("Fourth upconv input: " + str(fourthUpconvInputChannels))
        #         # Logging.log("Fourth upconv output: " + str(fourthUpconvOutputChannels))
        self.fourthUpConv = BaselineUpconvBlock(fourthUpconvInputChannels,
                                        fourthUpconvOutputChannels,
                                        firstConvOutChannels=fourthUpconvOutputChannels,
                                        secondConvOutChannels=fourthUpconvOutputChannels,
                                        kernelSize=3,
                                        stride=2,
                                        padding=0,
                                        includeFinalBatchNorm=True).to(device)

        # self.firstConvAndPoolCombinationConv = nn.Conv2d(fourthUpconvOutputChannels, fourthUpconvOutputChannels, kernel_size=1)
        # self.reduceFirstConvOutput = nn.Conv2d(fourthUpconvOutputChannels, fourthUpconvOutputChannels // 2, kernel_size=1)
        # self.reduceInitialBlockOutput = nn.Conv2d(fourthUpconvOutputChannels, fourthUpconvOutputChannels // 2, kernel_size=1)
        # secondDimensionAugmentationOutputChannels = fourthUpconvOutputChannels * 2
        # self.secondDimensionAugmentationLayer = nn.Conv2d(fourthUpconvOutputChannels, secondDimensionAugmentationOutputChannels, kernel_size=1)
        # fifthUpconvInputChannels = firstDimensionAugmentationOutputChannels


        # #         # Logging.log("Fourth upconv output channels: " + str(fourthUpconvOutputChannels))

        # self.preFinalUpconvBatchNorm = nn.BatchNorm2d(num_features=fourthUpconvOutputChannels)

        finalUpconvOutputChannels = fourthUpconvOutputChannels // 2
        finalKernelSize = 7
        self.finalUpconv = BaselineUpconvBlock(fourthUpconvOutputChannels,
                                               finalUpconvOutputChannels,
                                               finalUpconvOutputChannels,
                                               finalUpconvOutputChannels,
                                               kernelSize=finalKernelSize,
                                               padding=0,
                                               stride=2,
                                               includeFinalBatchNorm=False)
            # nn.ConvTranspose2d(in_channels=fourthUpconvOutputChannels,
            #                                   out_channels=finalUpconvOutputChannels,
            #                                   kernel_size=finalKernelSize,
            #                                   stride=2,
            #                                   padding=0,
            #                                   includ)

        # self.enlargingConv = nn.Conv2d(in_channels=finalUpconvOutputChannels,
        #                               out_channels=finalUpconvOutputChannels,
        #                               kernel_size=2,
        #                               padding=4)

        self.predictionConv = nn.Conv2d(in_channels=finalUpconvOutputChannels, out_channels=numClasses, kernel_size=1)
        self.ceLoss = nn.CrossEntropyLoss(ignore_index=ignoreIndex, reduction='mean')

        # self.reduceConvDimensionsByHalf = nn.Conv2d(ModifiedResnet.INITIAL_BLOCK_OUTPUT_CHANNELS,
        #                                             ModifiedResnet.INITIAL_BLOCK_OUTPUT_CHANNELS,
        #                                             kernel_size=2,
        #                                             padding=2,
        #                                             stride=2)

        # self.upconvInitialPoolByTwo = nn.ConvTranspose2d(in_channels=ModifiedResnet.INITIAL_BLOCK_OUTPUT_CHANNELS // 2,
        #                                                  out_channels=ModifiedResnet.INITIAL_BLOCK_OUTPUT_CHANNELS // 2,
        #                                                  kernel_size=3,
        #                                                  stride=2,
        #                                                  padding=0)

    #     inChannels: int,
    #                  upConvOutChannels: int,
    #                  firstConvOutChannels: int,
    #                  secondConvOutChannels: int,
    #                  kernelSize: int,
    #                  padding: int,
    #                  stride: int,
    def forward(self, x, y):
        #         # Logging.log("Resnet input: " + str(x.size()))
        #         # Logging.log("Labels: " + str(y.size()))
        rawPredictions = self.makeRawPredictions(x)
        # #         # Logging.log("Predictions: " + str(rawPredictions.size()))

        loss = LossComputations.computeLoss(rawPredictions, y, self.ceLoss)
        # #         # Logging.log("Loss: " + str(loss))
        # #         # Logging.log("Loss type: " + str(type(loss)))
        return loss

    def makeRawPredictions(self, x: torch.Tensor) -> torch.Tensor:
        # Logging.log("Sending inputs through resnet")
        firstConvOutput, initialBlockOutput, firstBlockOutput, secondBlockOutput, thirdBlockOutput, fourthBlockOutput = self.modifiedResnet(x)
        firstConvOutput = initialBlockOutput = firstBlockOutput = secondBlockOutput = thirdBlockOutput = None
        # Logging.log("Fourth block output: " + str(fourthBlockOutput.size()))
        # Logging.log("Finished with resnet")
        # # Logging.log("Initial block output: " + str(initialBlockOutput.size()))
        # # Logging.log("First conv output: " + str(firstConvOutput.size()))

        #         #         # Logging.log("Resnet output: " + str(fourthBlockOutput.size()))

        result = self.firstUpConv(fourthBlockOutput)
        fourthBlockOutput = None
        thirdBlockOutput = None


        # Logging.log("First upconv: " + str(result.size()))
        result = self.secondUpConv(result)
        secondBlockOutput = None
        # Logging.log("Second upconv: " + str(result.size()))

        result = self.thirdUpConv(result)
        firstBlockOutput = None
        # Logging.log("Third upconv: " + str(result.size()))

        # if self.resnetType == ModifiedResnet.ResnetType.FULL_101:
        # result = self.firstDimensionAugmentationLayer(result)
        # Logging.log("Augmented size: " + str(result.size()))

        # fourthUpconv = self.fourthUpConv(result, initialBlockOutput)
        #         # Logging.log("Augmented third upconv: : " + str(reducedThirdUpconv.size()))

        # firstConvOutput = self.reduceFirstConvOutput(firstConvOutput)
        # initialBlockOutput = self.reduceInitialBlockOutput(initialBlockOutput)
        # Logging.log("First conv output: " + str(firstConvOutput.size()))
        # Logging.log(("Initial block: " + str(initialBlockOutput.size())))
        # concatenatedFirstConvAndPool = self.combineFirstResnetConvAndPool(firstConvOutput, initialBlockOutput)

        # Logging.log("Concatenated first conv and pool: " + str(concatenatedFirstConvAndPool.size()))
        # combinedFirstConvAndPool = self.firstConvAndPoolCombinationConv(concatenatedFirstConvAndPool)
        concatenatedFirstConvAndPool = None
        firstConvOutput = None
        initialBlockOutput = None
        # Logging.log("Combined first conv and pool: " + str(combinedFirstConvAndPool.size()))

        result = self.fourthUpConv(result)
        combinedFirstConvAndPool = None

        # Logging.log("Fourth upconv: " + str(result.size()))

        # result = self.preFinalUpconvBatchNorm(result)

        # Logging.log("Final batch norm: " + str(result.size()))

        result = self.finalUpconv(result)
        # Logging.log("Final upconv: " + str(result.size()))

        result = self.predictionConv(result)
        return result

    def predict(self, x: torch.Tensor, heightAndWidth: Tuple[int, int]) -> torch.Tensor:
        rawPredictions = self.makeRawPredictions(x)
        yH, yW = heightAndWidth
        hDifference = x.shape[2] - yH
        wDifference = x.shape[3] - yW
        rawPredictions = rawPredictions[:, :, hDifference // 2:yH + (hDifference - hDifference //2), wDifference // 2:yW + (wDifference - wDifference //2)]

        # torch.set_printoptions(profile="full")
        # print(rawPredictions.size())
        # print(torch.argmax(rawPredictions, dim=1))
        # torch.set_printoptions(profile="default")
        return torch.squeeze(torch.argmax(rawPredictions, dim=1))

    # def unetDecoder(self, x: torch.Tensor) -> torch.Tensor:
        # Sequential(
#   (0): BasicBlock(
#     (conv1): Conv2d(256, 512, kernel_size=(3, 3), stride=(2, 2), padding=(1, 1), bias=False)
#     (bn1): BatchNorm2d(512, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
#     (relu): ReLU(inplace)
#     (conv2): Conv2d(512, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False)
#     (bn2): BatchNorm2d(512, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
#     (downsample): Sequential(
#       (0): Conv2d(256, 512, kernel_size=(1, 1), stride=(2, 2), bias=False)
#       (1): BatchNorm2d(512, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
#     )
#   )
#   (1): BasicBlock(
#     (conv1): Conv2d(512, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False)
#     (bn1): BatchNorm2d(512, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
#     (relu): ReLU(inplace)
#     (conv2): Conv2d(512, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False)
#     (bn2): BatchNorm2d(512, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
#   )
#   (2): BasicBlock(
#     (conv1): Conv2d(512, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False)
#     (bn1): BatchNorm2d(512, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
#     (relu): ReLU(inplace)
#     (conv2): Conv2d(512, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False)
#     (bn2): BatchNorm2d(512, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
#   )
# )





