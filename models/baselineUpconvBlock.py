import torch.nn as nn
import torch
from customlogging import Logging
import torch.nn.functional as F
from common.tensorOperations import TensorOperations
from models.convbatchrelu import ConvBatchRelu


class BaselineUpconvBlock(nn.Module):

    def __init__(self, inChannels: int,
                 upConvOutChannels: int,
                 firstConvOutChannels: int,
                 secondConvOutChannels: int,
                 kernelSize: int,
                 padding: int,
                 stride: int,
                 convKernelSize: int = 3,
                 convPadding: int = 1,
                 convStride: int = 1,
                 includeFinalBatchNorm: bool = False):
        super(BaselineUpconvBlock, self).__init__()
        self.upConv = nn.ConvTranspose2d(in_channels=inChannels, out_channels=upConvOutChannels, kernel_size=kernelSize, stride=stride, padding=padding, output_padding=(0, stride - 1))
        self.firstBatchNorm = nn.BatchNorm2d(num_features=upConvOutChannels)
        # nn.init.xavier_normal_(self.upConv.weight)

        self.firstCBR = ConvBatchRelu(upConvOutChannels, firstConvOutChannels, convKernelSize, convPadding, convStride)

    # self.firstConv = nn.Conv2d(upConvOutChannels, firstConvOutChannels, kernel_size=convKernelSize, padding=convPadding, stride=convStride)
    #     self.secondBatchNorm = nn.BatchNorm2d(num_features=firstConvOutChannels)
    #     self.secondCBR = ConvBatchRelu(firstConvOutChannels, secondConvOutChannels, kernelSize=convKernelSize, padding=convPadding, stride=convStride)
        # self.secondConv = nn.Conv2d(firstConvOutChannels, secondConvOutChannels, kernel_size=convKernelSize, padding=convPadding, stride=convStride)
        # self.thirdBatchNorm = nn.BatchNorm2d(num_features=secondConvOutChannels)

        # self.thirdConv = nn.Conv2d(secondConvOutChannels, secondConvOutChannels, kernel_size=convKernelSize, padding=convPadding, stride=convStride)
        #
        # self.fourthBatchNorm = None
        # if includeFinalBatchNorm:
        #     self.fourthBatchNorm = nn.BatchNorm2d(secondConvOutChannels)

        # nn.init.kaiming_normal_(self.firstConv.weight, nonlinearity="relu")
        # nn.init.kaiming_normal_(self.secondConv.weight, nonlinearity="relu")
        # nn.init.kaiming_normal_(self.thirdConv.weight, nonlinearity="relu")

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        # Logging.log("Input: " + str(x.size()))
        # Logging.log("Candidate size: " + str(outputToConcat.size()))
        x = self.upConv(x)
        # Logging.log("Upconv: " + str(x.size()))
        # if outputToConcat is not None:
        # x = TensorOperations.concatenateXAndOther(x, outputToConcat)
        x = self.firstBatchNorm(x)
        # x = self.firstConv(x)
        # x = F.relu_(x)
        # x = self.secondBatchNorm(x)
        # x = F.relu_(x)
        x = self.firstCBR(x)
        # x = self.secondCBR(x)
        # Logging.log("First conv: " + str(x.size()))
        # x = self.secondConv(x)
        # x = self.thirdBatchNorm(x)
        # x = F.relu_(x)
        # x = self.thirdConv(x)
        # x = F.relu_(x)
        # if self.fourthBatchNorm is not None:
        #     x = self.fourthBatchNorm(x)
        #     x = F.relu_(x)
        # Logging.log("Second conv: " + str(x.size()))

        return x



# import torch.nn as nn
#
# import torch
# from customlogging import Logging
# import torch.nn.functional as F
# from common.tensorOperations import TensorOperations
#
#
# class BaselineUpconvBlock(nn.Module):
#
#     def __init__(self, inChannels: int,
#                  upConvOutChannels: int,
#                  firstConvOutChannels: int,
#                  secondConvOutChannels: int,
#                  kernelSize: int,
#                  padding: int,
#                  stride: int,
#                  convKernelSize: int = 3,
#                  convPadding: int = 1,
#                  convStride: int = 1,
#                  includeFinalBatchNorm: bool = False):
#         super(BaselineUpconvBlock, self).__init__()
#         self.upConv = nn.ConvTranspose2d(in_channels=inChannels, out_channels=upConvOutChannels, kernel_size=kernelSize, stride=stride, padding=padding, output_padding=(0, stride - 1))
#         self.firstBatchNorm = nn.BatchNorm2d(num_features=upConvOutChannels)
#         self.firstConv = nn.Conv2d(upConvOutChannels, firstConvOutChannels, kernel_size=convKernelSize, padding=convPadding, stride=convStride)
#         self.secondBatchNorm = nn.BatchNorm2d(num_features=firstConvOutChannels)
#
#
#         self.firstConv = nn.Conv2d(upConvOutChannels, secondConvOutChannels, kernel_size=convKernelSize, padding=convPadding, stride=convStride)
#         # self.secondBatchNorm = nn.BatchNorm2d(num_features=secondConvOutChannels)
#         # self.secondConv = nn.Conv2d(firstConvOutChannels, secondConvOutChannels, kernel_size=convKernelSize, padding=convPadding, stride=convStride)
#
#         # self.thirdBatchNorm = None
#         # if includeFinalBatchNorm:
#         #     self.thirdBatchNorm = nn.BatchNorm2d(secondConvOutChannels)
#
#         nn.init.kaiming_normal_(self.firstConv.weight, nonlinearity="relu")
#         # nn.init.kaiming_normal_(self.secondConv.weight, nonlinearity="relu")
#
#     def forward(self, x: torch.Tensor) -> torch.Tensor:
#         # Logging.log("Input: " + str(x.size()))
#         # Logging.log("Candidate size: " + str(outputToConcat.size()))
#         x = self.upConv(x)
#         # Logging.log("Upconv: " + str(x.size()))
#         # if outputToConcat is not None:
#         # x = TensorOperations.concatenateXAndOther(x, outputToConcat)
#         x = self.firstBatchNorm(x)
#         x = self.firstConv(x)
#         x = F.relu_(x)
#         # x = self.secondBatchNorm(x)
#         # Logging.log("First conv: " + str(x.size()))
#         # x = self.secondConv(x)
#         # x = F.relu_(x)
#         # if self.thirdBatchNorm is not None:
#         #     x = self.thirdBatchNorm(x)
#         # Logging.log("Second conv: " + str(x.size()))
#
#         return x