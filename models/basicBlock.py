import torch.nn as nn
import torch
from customlogging import Logging
from models.convbatchrelu import ConvBatchRelu


class BasicBlock(nn.Module):
    def __init__(self, inChannels: int,
                 kernelSize=3,
                 convStride: int = 1,
                 convPadding: int = 1,
                 includeConvBias: bool = False,
                 includeSkipConnection: bool = True):
        super(BasicBlock, self).__init__()
        self.firstCRB = ConvBatchRelu(inChannels, inChannels, kernelSize=kernelSize, stride=convStride, padding=convPadding, includeConvBias=includeConvBias)
        self.secondCRB = ConvBatchRelu(inChannels, inChannels, kernelSize=kernelSize, stride=convStride, padding=convPadding, includeConvBias=includeConvBias, includeRelu=False)
        self.includeSkipConnection = includeSkipConnection

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        if self.includeSkipConnection:
            return x + self.secondCRB(self.firstCRB(x))
        else:
            x = self.firstCRB(x)
            x = self.secondCRB(x)
            return x

