import torch
import torch.nn as nn
from models.convbatchrelu import ConvBatchRelu
from common.tensorOperations import TensorOperations
from customlogging import Logging


class ConcatCRBTranspose(nn.Module):

    def __init__(self, inChannels: int,
                 outChannels: int,
                 convKernelSize: int,
                 convPadding: int = 0,
                 convStride: int = 1,
                 transposeKernel: int = 3,
                 transposePadding: int = 0,
                 transposeStride: int = 2,
                 includeRelu: bool = True,
                 includeBatchNorm: bool = True,
                 includeConvBias: bool = True,
                 includeTransposeBias: bool = True):
        super(ConcatCRBTranspose, self).__init__()
        self.crb = ConvBatchRelu(inChannels * 2, inChannels, convKernelSize, convPadding, convStride, includeBatchNorm=includeBatchNorm, includeRelu=includeRelu, includeConvBias=includeConvBias)
        self.tranpose = nn.ConvTranspose2d(inChannels, outChannels, kernel_size=transposeKernel, stride=transposeStride, padding=transposePadding, bias=includeTransposeBias)
        nn.init.xavier_normal_(self.tranpose.weight)

    def forward(self, x: torch.Tensor, concat: torch.Tensor) -> torch.Tensor:
        # Logging.log("Input: " + str(x.size()))
        # Logging.log("Concat: " + str(concat.size()))
        x = self.crb(TensorOperations.concatenateXAndOther(x, concat))
        concat = None
        x = self.tranpose(x)
        return x

