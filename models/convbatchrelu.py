import torch
import torch.nn as nn
import torch.nn.functional as F
from customlogging import Logging

class ConvBatchRelu(nn.Module):

    def __init__(self, inChannels: int,
                 outChannels: int,
                 kernelSize: int,
                 padding: int = 0,
                 stride: int = 1,
                 includeBatchNorm: bool = True,
                 includeRelu: bool = True,
                 includeConvBias: bool = True):
        super(ConvBatchRelu, self).__init__()
        self.conv = nn.Conv2d(inChannels, outChannels, kernel_size=kernelSize, padding=padding, stride=stride, bias=includeConvBias)
        self.bn = None
        if includeBatchNorm:
            self.bn = nn.BatchNorm2d(num_features=outChannels)
        self.includeRelu = includeRelu

        if includeRelu:
            nn.init.kaiming_normal_(self.conv.weight, nonlinearity="relu")
        else:
            nn.init.xavier_normal_(self.conv.weight)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        x = self.conv(x)
        if self.bn is not None:
            x = self.bn(x)
        if self.includeRelu:
            x = F.relu_(x)

        return x

