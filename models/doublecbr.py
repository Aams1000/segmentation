import torch.nn as nn
import torch
from customlogging import Logging
from models.convbatchrelu import ConvBatchRelu


class DoubleCBR(nn.Module):
    def __init__(self, inChannels: int,
                 outChannels: int,
                 kernelSize=3,
                 convStride: int = 1,
                 convPadding: int = 1,
                 includeConvBias: bool = False):
        super(DoubleCBR, self).__init__()
        self.firstCRB = ConvBatchRelu(inChannels, inChannels, kernelSize=kernelSize, stride=convStride, padding=convPadding, includeConvBias=includeConvBias)
        self.secondCRB = ConvBatchRelu(inChannels, outChannels, kernelSize=kernelSize, stride=convStride, padding=convPadding, includeConvBias=includeConvBias)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        x = self.firstCRB(x)
        x = self.secondCRB(x)
        return x

