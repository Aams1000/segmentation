import torch
from customlogging import Logging
import torch.nn as nn
from PIL import Image, ImageOps
from fileio.CustomFileIO import CustomFileIO
from fileio.datasets import Dataset


class LossComputations(object):

    @staticmethod
    def computeLoss(predictions: torch.Tensor, y: torch.Tensor, lossFunction: nn.Module, showPredictions: bool = False) -> torch.Tensor:

        if showPredictions:
            LossComputations.__showImage(torch.argmax(predictions, dim=1))
        try:
            return lossFunction(predictions, y)
        except Exception as e:
            torch.set_printoptions(profile="full")
            # Logging.log(y)
            torch.set_printoptions(profile="default")
            raise e


    @staticmethod
    def __showImage(predictionTensor: torch.Tensor) -> None:
        predictionTensor = torch.squeeze(predictionTensor).long()
        nonZeroMask = (predictionTensor > 0).long()
        predictionTensor = (predictionTensor + 91) * nonZeroMask
        npImage = predictionTensor.numpy()

        labelImage = CustomFileIO.loadAndResizePilImage(CustomFileIO.DataType.TOY,
                                                        "000000000139",
                                                        CustomFileIO.ImageType.LABEL,
                                                        Dataset.RESNET_INPUT_SIZE,
                                                        ".png",
                                                        False)
        predictionImage = Image.fromarray(npImage.astype('uint8'), 'P')
        predictionImage = ImageOps.expand(predictionImage, 0)
        predictionImage.putpalette(labelImage.palette.palette)
        predictionImage.show()