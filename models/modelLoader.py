from typing import *
import torch.nn as nn
import torch.optim
from models.unetModel import UNetModel
from models.baselineModel import BaselineModel
from models.resnetUnet import ResnetUnet
from distutils.util import strtobool
from customlogging import Logging
from clargs import CLArgs
from models.modifiedResnet import ModifiedResnet


class ModelLoader(object):

    @staticmethod
    def loadModelOptimizerAndResnet(modelType: str,
                                    learningRate: float,
                                    regularizationStrength: float,
                                    numClasses: int,
                                    ignoreIndex: int,
                                    resnetType: int,
                                    device: torch.device,
                                    modelStateDict: Dict = None,
                                    optimizerStateDict = None,
                                    parallelize: bool = True) -> Tuple[nn.Module, torch.optim.Optimizer, nn.Module]:
        model = ModelLoader.__loadModel(modelType, numClasses, ignoreIndex, resnetType, device).to(device)
        if parallelize:
            Logging.log("Configuring model for multi-GPU support")
            model = nn.DataParallel(model)
        else:
            Logging.log("WARNING: You have explicitly selected a non-multi-GPU-enabled model. Enter [y] to confirm and proceed, enter [n] to continue with a multi-GPU-enabled version.")
            waitingOnResponse = True
            while waitingOnResponse:
                choice = input().lower()
                try:
                    if strtobool(choice):
                        Logging.log("Non-multi-GPU preference confirmed. Continuing without multi-GPU support.")
                    else:
                        Logging.log("Setting model to support multiple GPUs. Proceeding.")
                        model = nn.DataParallel(model)
                    waitingOnResponse = False
                except Exception as e:
                    Logging.log("Invalid input. Please try again.")

        optimizer = torch.optim.Adam(model.parameters(), lr=learningRate, weight_decay=regularizationStrength)

        # Make sure these come AFTER wrapping with DataParallel!
        if modelStateDict is not None:
            Logging.log("Loading previously existing model state dict")
            model.load_state_dict(modelStateDict)
        if optimizerStateDict is not None:
            Logging.log("Loading previously existing optimizer state dict")
            optimizer.load_state_dict(optimizerStateDict)
        return model, optimizer, ModifiedResnet(type=resnetType).to(device)

    @staticmethod
    def __loadModel(modelType: str, numClasses: int, ignoreIndex: int, resnetType: int, device: torch.device) -> nn.Module:
        if CLArgs.ModelType.UNET == modelType:
            return ModelLoader.__loadStandardModel(numClasses, ignoreIndex, resnetType, device)
        elif CLArgs.ModelType.BASELINE == modelType:
            return ModelLoader.__loadBaselineModel(numClasses, ignoreIndex, resnetType, device)
        elif CLArgs.ModelType.RESNET_UNET == modelType:
            return ModelLoader.__loadResnetUnet(numClasses, ignoreIndex, resnetType)
        else:
            raise ValueError("Invalid model type: " + str(modelType))

    @staticmethod
    def __loadStandardModel(numClasses: int, ignoreIndex: int, resnetType: int, device: torch.device) -> nn.Module:
        return UNetModel(numClasses, ignoreIndex, resnetType, device)

    @staticmethod
    def __loadBaselineModel(numClasses: int, ignoreIndex: int, resnetType: int, device: torch.device) -> nn.Module:
        return BaselineModel(numClasses, ignoreIndex, resnetType, device)

    @staticmethod
    def __loadResnetUnet(numClasses: int, ignoreIndex: int, resnetType: int) -> nn.Module:
        return ResnetUnet(numClasses, ignoreIndex, resnetType)
