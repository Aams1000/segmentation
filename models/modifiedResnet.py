import torch.nn as nn
import torch
from torchvision.models.resnet import ResNet, resnet101, resnet34, resnet50
from typing import *
from customlogging import Logging

class ModifiedResnet(nn.Module):

    class ResnetType:
        FULL_101 = 101
        SMALL_34 = 34

    INITIAL_BLOCK_OUTPUT_CHANNELS = 64

    FINAL_NUM_CHANNELS_101 = 2048
    FINAL_NUM_CHANNELS_34 = 512

    def __init__(self, type: int):
        super(ModifiedResnet, self).__init__()
        self.type = None
        if type == ModifiedResnet.ResnetType.FULL_101 or type == ModifiedResnet.ResnetType.SMALL_34:
            self.type = type
        else:
            raise ValueError("Illegal ResNet type " + str(type) + " passed to ModifiedResnet constructor.")

        pretrainedResnet = ModifiedResnet.__loadPretrainedResnet(type)
        self.conv1 = pretrainedResnet.conv1
        self.bn1 = pretrainedResnet.bn1
        self.relu = pretrainedResnet.relu
        self.maxpool = pretrainedResnet.maxpool

        self.layer1 = pretrainedResnet.layer1
        self.layer2 = pretrainedResnet.layer2
        self.layer3 = pretrainedResnet.layer3
        self.layer4 = pretrainedResnet.layer4
        self.avgpool = pretrainedResnet.avgpool


    def forward(self, x) -> Tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor]:
        firstConvOutput = self.conv1(x)
        x = self.bn1(firstConvOutput)
        x = self.relu(x)
        initialBlockOutput = self.maxpool(x)
        # Logging.log("Initial: " + str(initialBlockOutput.size()))

        firstBlockOutput = self.layer1(initialBlockOutput)
        # Logging.log("First block: " + str(firstBlockOutput.size()))
        secondBlockOutput = self.layer2(firstBlockOutput)
        # Logging.log("Second block: " + str(secondBlockOutput.size()))
        thirdBlockOutput = self.layer3(secondBlockOutput)
        # Logging.log("Third block: " + str(thirdBlockOutput.size()))
        fourthBlockOutput = self.layer4(thirdBlockOutput)
        # Logging.log("Fourth block: " + str(fourthBlockOutput.size()))
        # Logging.log("Average pool size: " + str(self.avgpool(fourthBlockOutput).size()))

        return firstConvOutput, initialBlockOutput, firstBlockOutput, secondBlockOutput, thirdBlockOutput, fourthBlockOutput


    @staticmethod
    def __loadPretrainedResnet(type: int) -> ResNet:
        rawResnet = resnet34(pretrained=True) if type == ModifiedResnet.ResnetType.SMALL_34 else resnet50(pretrained=True)

        for param in rawResnet.parameters():
            param.requires_grad = False

        # SUPER IMPORTANT
        rawResnet.eval()
        return rawResnet


    def main(self) -> None:
        resnet = ModifiedResnet.__loadPretrainedResnet(ModifiedResnet.ResnetType.FULL_101)
        modifiedResnet = ModifiedResnet(ModifiedResnet.ResnetType.FULL_101)
        i = 5

if __name__ == "__main__":
    ModifiedResnet(ModifiedResnet.ResnetType.SMALL_34).main()
