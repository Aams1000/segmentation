import torch.nn as nn
import torch
from customlogging import Logging
from models.basicBlock import BasicBlock


class NonTransposeResnetSection(nn.Module):
    def __init__(self, inChannels: int,
                 outChannels: int,
                 kernelSize=3,
                 convStride: int = 1,
                 convPadding: int = 1,
                 includeConvBias: bool = False):
        super(NonTransposeResnetSection, self).__init__()

        self.firstBlock = BasicBlock(inChannels, kernelSize=kernelSize, convStride=convStride, convPadding=convPadding, includeConvBias=includeConvBias)
        self.secondBlock = BasicBlock(inChannels, kernelSize=kernelSize, convStride=convStride, convPadding=convPadding, includeConvBias=includeConvBias)
        self.thirdBlock = BasicBlock(inChannels, kernelSize=kernelSize, convStride=convStride, convPadding=convPadding, includeConvBias=includeConvBias)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        x = self.firstBlock(x)
        x = self.secondBlock(x)
        x = self.thirdBlock(x)

        return x

