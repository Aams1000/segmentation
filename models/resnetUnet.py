import torch.nn as nn
from models.modifiedResnet import ModifiedResnet
from customlogging import Logging
import torch
from typing import *
from models.lossComputations import LossComputations
from models.transposeResnetSection import TransposeResnetSection
from models.nonTransposeResnetSection import NonTransposeResnetSection
from models.convbatchrelu import ConvBatchRelu
from models.concatCRBTranpose import ConcatCRBTranspose


class ResnetUnet(nn.Module):

    def __init__(self, numClasses: int, ignoreIndex: int, resnetType: int):
        super(ResnetUnet, self).__init__()
        self.resnetType = resnetType

        decoderOutputChannels = ModifiedResnet.FINAL_NUM_CHANNELS_34 if resnetType == ModifiedResnet.ResnetType.SMALL_34 else ModifiedResnet.FINAL_NUM_CHANNELS_101

        self.fourthBlock = TransposeResnetSection(decoderOutputChannels,
                                                  decoderOutputChannels // 2)
        self.thirdBlock = TransposeResnetSection(decoderOutputChannels // 2,
                                                 decoderOutputChannels // 4,
                                                 additionalBlocks=3)
        self.secondBlock = TransposeResnetSection(decoderOutputChannels // 4,
                                                  decoderOutputChannels // 8,
                                                  additionalBlocks=1)

        self.firstBlock = NonTransposeResnetSection(decoderOutputChannels // 8,
                                                    decoderOutputChannels // 8)

        self.maxPoolCRBTranspose = ConcatCRBTranspose(decoderOutputChannels // 8, decoderOutputChannels // 8, convKernelSize=3, convPadding=1, includeConvBias=False, includeTransposeBias=False, transposeKernel=2)

        self.firstPostMaxPoolCRB = ConvBatchRelu(decoderOutputChannels // 8, decoderOutputChannels // 8, kernelSize=3, padding=1, stride=1, includeConvBias=False)
        self.secondPostMaxPoolCRB = ConvBatchRelu(decoderOutputChannels // 8, decoderOutputChannels // 8, kernelSize=3, padding=1, stride=1, includeConvBias=False)

        self.initialConvCRBTranspose = ConcatCRBTranspose(decoderOutputChannels // 8, decoderOutputChannels // 16, convKernelSize=3, convPadding=1, includeConvBias=False, includeTransposeBias=False, transposeKernel=2)
        self.penultimateConv = ConvBatchRelu(decoderOutputChannels // 16, decoderOutputChannels // 16, kernelSize=5, padding=2, stride=1, includeConvBias=False)

        self.predictionConv = nn.Conv2d(in_channels=decoderOutputChannels // 16, out_channels=numClasses, kernel_size=1)
        self.ceLoss = nn.CrossEntropyLoss(ignore_index=ignoreIndex, reduction='mean')

    def forward(self, resnetOutputs: Tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor], y: torch.Tensor, showPrediction: bool = False):
        rawPredictions = self.makeRawPredictions(resnetOutputs)
        loss = LossComputations.computeLoss(rawPredictions, y, self.ceLoss, showPrediction)
        return loss

    def makeRawPredictions(self, resnetOutputs: Tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor]) -> torch.Tensor:
        # Logging.log("Sending inputs through resnet")
        firstConvOutput, maxPoolOutput, firstBlockOutput, secondBlockOutput, thirdBlockOutput, fourthBlockOutput = resnetOutputs
        # Logging.log("Initial block output: " + str(maxPoolOutput.size()))
        # Logging.log("First conv output: " + str(firstConvOutput.size()))

        result = self.fourthBlock(fourthBlockOutput, thirdBlockOutput)
        fourthBlockOutput = None
        thirdBlockOutput = None

        # Logging.log("First block: " + str(result.size()))
        result = self.thirdBlock(result, secondBlockOutput)
        secondBlockOutput = None
        # Logging.log("Second block: " + str(result.size()))

        result = self.secondBlock(result, firstBlockOutput)
        firstBlockOutput = None
        # Logging.log("Third block: " + str(result.size()))

        result = self.firstBlock(result)
        combinedFirstConvAndPool = None

        result = self.maxPoolCRBTranspose(result, maxPoolOutput)
        result = self.firstPostMaxPoolCRB(result)
        result = self.secondPostMaxPoolCRB(result)
        maxPoolOutput = None
        # Logging.log("Max pool incorporated: " + str(result.size()))

        result = self.initialConvCRBTranspose(result, firstConvOutput)
        firstConvOutput = None
        # Logging.log("Initial conv incorporated: " + str(result.size()))

        result = self.penultimateConv(result)
        # Logging.log("Penultimate output: " + str(result.size()))

        result = self.predictionConv(result)
        return result

    def predict(self, resnetOutputs: Tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor]) -> torch.Tensor:
        return torch.squeeze(torch.argmax(self.makeRawPredictions(resnetOutputs), dim=1))


