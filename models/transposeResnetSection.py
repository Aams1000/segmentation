import torch.nn as nn
import torch
from customlogging import Logging
from models.convbatchrelu import ConvBatchRelu
from common.tensorOperations import TensorOperations
from models.basicBlock import BasicBlock


class TransposeResnetSection(nn.Module):
    def __init__(self, inChannels: int,
                 outChannels: int,
                 kernelSize=3,
                 convStride: int = 1,
                 convPadding: int = 1,
                 additionalBlocks: int = 0,
                 includeConvBias: bool = False,
                 transposeKernel: int = 3,
                 transposeStride: int = 2,
                 transposePadding: int = 1,
                 includeTransposeBias: bool = False):
        super(TransposeResnetSection, self).__init__()

        self.firstBlock = BasicBlock(inChannels, kernelSize=kernelSize, convStride=convStride, convPadding=convPadding, includeConvBias=includeConvBias)
        self.secondBlock = BasicBlock(inChannels, kernelSize=kernelSize, convStride=convStride, convPadding=convPadding, includeConvBias=includeConvBias)

        self.thirdBlock = None
        self.fourthBlock = None
        self.fifthBlock = None

        if additionalBlocks >= 1:
            self.thirdBlock = BasicBlock(inChannels, kernelSize=kernelSize, convStride=convStride, convPadding=convPadding, includeConvBias=includeConvBias)
        elif additionalBlocks == 3:
            self.fourthBlock = BasicBlock(inChannels, kernelSize=kernelSize, convStride=convStride, convPadding=convPadding, includeConvBias=includeConvBias)
            self.fifthBlock = BasicBlock(inChannels, kernelSize=kernelSize, convStride=convStride, convPadding=convPadding, includeConvBias=includeConvBias)

        self.finalCRB = ConvBatchRelu(inChannels, inChannels, kernelSize=kernelSize, stride=convStride, padding=convPadding, includeConvBias=includeConvBias)
        self.transpose = nn.ConvTranspose2d(inChannels, outChannels, transposeKernel, transposeStride, transposePadding, bias=includeTransposeBias)
        self.downSample = nn.Conv2d(inChannels, outChannels, kernel_size=1, stride=1, padding=0, bias=False)

        nn.init.xavier_normal_(self.downSample.weight)

    def forward(self, x: torch.Tensor, concat: torch.Tensor) -> torch.Tensor:
        x = self.firstBlock(x)
        x = self.secondBlock(x)
        if self.thirdBlock is not None:
            x = self.thirdBlock(x)
        if self.fourthBlock is not None and self.fifthBlock is not None:
            x = self.fourthBlock(x)
            x = self.fifthBlock(x)
        x = self.finalCRB(x)
        x = self.transpose(x)
        x = self.downSample(TensorOperations.concatenateXAndOther(x, concat, shouldExpandX=True))

        return x
