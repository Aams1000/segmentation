import torch.nn as nn
from models.modifiedResnet import ModifiedResnet
from models.upconv import UpconvBlock
from customlogging import Logging
import torch
from common.tensorOperations import TensorOperations
from typing import *
from models.lossComputations import LossComputations
from models.convbatchrelu import ConvBatchRelu
from models.doublecbr import DoubleCBR


class UNetModel(nn.Module):

    def __init__(self, numClasses: int, ignoreIndex: int, resnetType: int, device: torch.device):
        super(UNetModel, self).__init__()
        self.resnetType = resnetType

        firstUpconvInputChannels = ModifiedResnet.FINAL_NUM_CHANNELS_34 if resnetType == ModifiedResnet.ResnetType.SMALL_34 else ModifiedResnet.FINAL_NUM_CHANNELS_101
        firstUpconvOutputChannels = firstUpconvInputChannels // 2

        self.firstUpConv = UpconvBlock(firstUpconvInputChannels,
                                              firstUpconvOutputChannels,
                                              firstConvOutChannels=firstUpconvInputChannels,
                                              secondConvOutChannels=firstUpconvOutputChannels,
                                              kernelSize=3,
                                              stride=2,
                                              padding=1).to(device)

        secondUpconvOutputChannels = firstUpconvOutputChannels // 2
        self.secondUpConv = UpconvBlock(firstUpconvOutputChannels,
                                               secondUpconvOutputChannels,
                                               firstConvOutChannels=firstUpconvOutputChannels,
                                               secondConvOutChannels=secondUpconvOutputChannels,
                                               kernelSize=3,
                                               stride=2,
                                               padding=1).to(device)

        thirdUpconvOutputChannels = secondUpconvOutputChannels // 2
        self.thirdUpConv = UpconvBlock(secondUpconvOutputChannels,
                                              thirdUpconvOutputChannels,
                                              firstConvOutChannels=secondUpconvOutputChannels,
                                              secondConvOutChannels=thirdUpconvOutputChannels,
                                              kernelSize=3,
                                              stride=2,
                                              padding=1).to(device)

        fourthUpconvInputChannels = thirdUpconvOutputChannels
        if resnetType == ModifiedResnet.ResnetType.FULL_101:
            firstDimensionAugmentationOutputChannels = thirdUpconvOutputChannels // 2
            self.firstDimensionAugmentationLayer = nn.Conv2d(thirdUpconvOutputChannels, firstDimensionAugmentationOutputChannels, kernel_size=1)
            fourthUpconvInputChannels = firstDimensionAugmentationOutputChannels
        else:
            firstDimensionAugmentationOutputChannels = thirdUpconvOutputChannels * 2
            self.firstDimensionAugmentationLayer = nn.Conv2d(thirdUpconvOutputChannels, firstDimensionAugmentationOutputChannels, kernel_size=1)
            fourthUpconvInputChannels = firstDimensionAugmentationOutputChannels

        fourthUpconvOutputChannels = fourthUpconvInputChannels // 2

        self.fourthUpConv = UpconvBlock(fourthUpconvInputChannels,
                                          fourthUpconvOutputChannels,
                                          firstConvOutChannels=fourthUpconvInputChannels,
                                          secondConvOutChannels=fourthUpconvOutputChannels,
                                          kernelSize=3,
                                          stride=2,
                                          padding=1).to(device)

        self.firstMaxPoolBlock = None
        self.secondMaxPoolBlock = None
        if self.resnetType == ModifiedResnet.ResnetType.FULL_101:
            self.firstMaxPoolBlock = DoubleCBR(secondUpconvOutputChannels // 2, secondUpconvOutputChannels // 4, includeConvBias=True)
            self.secondMaxPoolBlock = DoubleCBR(secondUpconvOutputChannels // 4, secondUpconvOutputChannels // 8, includeConvBias=True)

        self.concatenatedMaxPoolConv = ConvBatchRelu(fourthUpconvInputChannels, fourthUpconvOutputChannels * 2, kernelSize=3, padding=1, stride=1)
        self.firstConvOutputUpconv = UpconvBlock(fourthUpconvOutputChannels * 2,
                                                 fourthUpconvOutputChannels,
                                                 firstConvOutChannels=fourthUpconvOutputChannels * 2,
                                                 secondConvOutChannels=fourthUpconvOutputChannels,
                                                 kernelSize=3,
                                                 stride=2,
                                                 padding=1)

        self.preFinalUpconvBatchNorm = nn.BatchNorm2d(num_features=fourthUpconvOutputChannels)

        finalUpconvOutputChannels = fourthUpconvOutputChannels // 2
        finalKernelSize = 4
        self.finalUpconv = nn.ConvTranspose2d(in_channels=fourthUpconvOutputChannels,
                                              out_channels=finalUpconvOutputChannels,
                                              kernel_size=finalKernelSize,
                                              stride=2,
                                              padding=1)

        self.predictionConv = nn.Conv2d(in_channels=finalUpconvOutputChannels, out_channels=numClasses, kernel_size=1)
        self.ceLoss = nn.CrossEntropyLoss(ignore_index=ignoreIndex, reduction='mean')

    def forward(self, resnetOutputs: Tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor], y: torch.Tensor, showPrediction: bool = False):
        rawPredictions = self.makeRawPredictions(resnetOutputs)
        loss = LossComputations.computeLoss(rawPredictions, y, self.ceLoss, showPrediction)
        return loss

    def makeRawPredictions(self, resnetOutputs: Tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor]) -> torch.Tensor:
        firstConvOutput, maxPoolOutput, firstBlockOutput, secondBlockOutput, thirdBlockOutput, fourthBlockOutput = resnetOutputs

        # Logging.log("Fourth output: " + str(fourthBlockOutput.size()))
        # Logging.log("Third otuput: " + str(thirdBlockOutput.size()))
        result = self.firstUpConv(fourthBlockOutput, thirdBlockOutput)
        fourthBlockOutput = None
        thirdBlockOutput = None


        # Logging.log("First upconv: " + str(result.size()))
        result = self.secondUpConv(result, secondBlockOutput)
        secondBlockOutput = None
        # Logging.log("Second upconv: " + str(result.size()))

        result = self.thirdUpConv(result, firstBlockOutput)
        # Logging.log("First block output: " + str(firstBlockOutput.size()))
        firstBlockOutput = None
        # Logging.log("Third upconv: " + str(result.size()))

        if self.firstMaxPoolBlock is not None:
            result = self.firstMaxPoolBlock(result)
            # Logging.log("First MP reduction: " + str(result.size()))
            result = self.secondMaxPoolBlock(result)
            # Logging.log("Second MP reduction: " + str(result.size()))

        result = self.concatenatedMaxPoolConv(TensorOperations.concatenateXAndOther(result, maxPoolOutput))
        maxPoolOutput = None
        # Logging.log("Max pool incorporated: " + str(result.size()))

        # Logging.log("First conv: " + str(firstConvOutput.size()))
        result = self.firstConvOutputUpconv(result, firstConvOutput)
        firstConvOutput = None
        # Logging.log("First conv incorporated: " + str(result.size()))

        result = self.preFinalUpconvBatchNorm(result)
        # Logging.log("Final batch norm: " + str(result.size()))

        result = self.finalUpconv(result)
        # Logging.log("Final upconv: " + str(result.size()))

        result = self.predictionConv(result)
        return result

    def predict(self, resnetOutputs: Tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor]) -> torch.Tensor:
        return torch.squeeze(torch.argmax(self.makeRawPredictions(resnetOutputs), dim=1))

    def combineFirstResnetConvAndPool(self, firstConv: torch.Tensor, firstPool: torch.Tensor) -> torch.Tensor:
        return TensorOperations.concatenateXAndOther(firstConv, self.upconvInitialPoolByTwo(firstPool))
