import torch.nn as nn
import torch
from customlogging import Logging
import torch.nn.functional as F
from common.tensorOperations import TensorOperations


class UpconvBlock(nn.Module):

    def __init__(self, inChannels: int,
                 upConvOutChannels: int,
                 firstConvOutChannels: int,
                 secondConvOutChannels: int,
                 kernelSize: int,
                 padding: int,
                 stride: int,
                 convKernelSize: int = 3,
                 convPadding: int = 1,
                 convStride: int = 1):
        super(UpconvBlock, self).__init__()
        self.upConv = nn.ConvTranspose2d(in_channels=inChannels, out_channels=upConvOutChannels, kernel_size=kernelSize, stride=stride, padding=padding)
        self.firstBatchNorm = nn.BatchNorm2d(num_features=upConvOutChannels * 2)
        self.firstConv = nn.Conv2d(inChannels, firstConvOutChannels, kernel_size=convKernelSize, padding=convPadding, stride=convStride)
        self.secondBatchNorm = nn.BatchNorm2d(num_features=firstConvOutChannels)
        self.secondConv = nn.Conv2d(firstConvOutChannels, secondConvOutChannels, kernel_size=convKernelSize, padding=convPadding, stride=convStride)

        nn.init.kaiming_normal_(self.firstConv.weight, nonlinearity="relu")
        nn.init.kaiming_normal_(self.secondConv.weight, nonlinearity="relu")

    def forward(self, x: torch.Tensor, outputToConcat: torch.Tensor) -> torch.Tensor:
        # Logging.log("Input: " + str(x.size()))
        # Logging.log("Candidate size: " + str(outputToConcat.size()))
        x = self.upConv(x)
        # Logging.log("Upconv: " + str(x.size()))
        # if outputToConcat is not None:
        x = TensorOperations.concatenateXAndOther(x, outputToConcat, shouldExpandX=True)
        x = self.firstBatchNorm(x)
        x = self.firstConv(x)
        x = self.secondBatchNorm(x)
        x = F.relu_(x)
        # Logging.log("First conv: " + str(x.size()))
        x = self.secondConv(x)
        x = F.relu_(x)
        # Logging.log("Second conv: " + str(x.size()))

        return x

