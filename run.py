#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Usage:
    run.py [options]


Options:
    -h --help                               show this screen.
    --cuda                                  use GPU
    --train                                 Train on the full training set
    --test                                  Test on the full test set
    --toy                                   Train on the toy dataset
    --tiny                                  Train on one-example dataset
    --resnet101                             Use ResNet101 instead of standard ResNet34
    --resume                                Resume from pre-trained model
    --testOneImage                          Test and display one image from pretrained model
    --forceLr                               Force learning rate from config on checkpoint-loaded optimizer
    --baseline                              Use baseline model
    --unet                                  Use UNet model
    --resnetUnet                            Use ResnetUnet model
    --calculateMean                         Calculate training set per channel means and stds
    --validate                              Run validation
"""

from docopt import docopt

import time
import datetime
from typing import *

from customlogging import Logging
import torch
from torch.utils.data import DataLoader

from clargs import CLArgs
from config import Config
from fileio.datasets import Dataset
from fileio.CustomFileIO import CustomFileIO
from models.modelLoader import ModelLoader
from learningRateOperations import LROperations
from dataPreparation import DataPreparation
from validation import ValidationOperations
from testing import TestOperations
from models.modifiedResnet import ModifiedResnet

import numpy as np
from collections import deque


def calculateMeanAndStd(runId: str, runmode: str, device: torch.device) -> None:
    Logging.log("Commencing calculate means and stds run")
    datasetParameters = {'batch_size': Config.NN.BATCH_SIZE,
                         'shuffle': False,
                         'num_workers': Config.IO.NUM_DATALOADER_THREADS}

    trainingLoader = DataLoader(Dataset(CustomFileIO.DataType.TRAIN, torch.device(CLArgs.Device.CPU)), **datasetParameters)

    channel0Mean = 0
    channel1Mean = 0
    channel2Mean = 0

    channel0Sum = 0
    channel1Sum = 0
    channel2Sum = 0

    channel0VarSum = 0
    channel1VarSum = 0
    channel2VarSum = 0


    numSamples = 0
    totalPixels = Dataset.RESNET_INPUT_SIZE[0] * Dataset.RESNET_INPUT_SIZE[1]
    oneOverSqrtPixels = 1 / np.sqrt(totalPixels)

    for epoch in range(0, 2):
        Logging.log("Commencing epoch " + str(epoch))
        numSamples = 0
        for inputs, labels in trainingLoader:
            inputs = inputs.to(device)
            numSamples += inputs.shape[0]

            if epoch == 0:
                curr0Sum = inputs[:, 0, :, :].sum() / totalPixels
                curr1Sum = inputs[:, 1, :, :].sum() / totalPixels
                curr2Sum = inputs[:, 2, :, :].sum() / totalPixels
                channel0Sum += curr0Sum
                channel1Sum += curr1Sum
                channel2Sum += curr2Sum
                if numSamples % 10000 == 0:
                    Logging.log("Processed " + str(numSamples) + " samples, current means " + str(channel0Sum / numSamples) + ", " + str(channel1Sum / numSamples) + ", " + str(channel2Sum / numSamples))
            else:
                channel0VarSum += ((oneOverSqrtPixels * (inputs[:, 0, :, :] - channel0Mean))**2).sum()
                channel1VarSum += ((oneOverSqrtPixels * (inputs[:, 1, :, :] - channel1Mean))**2).sum()
                channel2VarSum += ((oneOverSqrtPixels * (inputs[:, 2, :, :] - channel2Mean))**2).sum()
                if numSamples % 10000 == 0:
                    Logging.log("Processed " + str(numSamples) + " samples, current stds " + str(torch.sqrt(channel0VarSum / numSamples)) + ", " + str(torch.sqrt(channel1VarSum / numSamples)) + ", " + str(torch.sqrt(channel2VarSum / numSamples)))

        if epoch == 0:
            Logging.log("Finished first pass")
            channel0Mean = channel0Sum / numSamples
            channel1Mean = channel1Sum / numSamples
            channel2Mean = channel2Sum / numSamples
            Logging.log("Channel 0 sum " + str(channel0Sum) + " , mean " + str(channel0Mean))
            Logging.log("Channel 1 sum " + str(channel1Sum) + " , mean " + str(channel1Mean))
            Logging.log("Channel 2 sum " + str(channel2Sum) + " , mean " + str(channel2Mean))
        else:
            Logging.log("Finished second pass")
            channel0Std = torch.sqrt(channel0VarSum / numSamples)
            channel1Std = torch.sqrt(channel1VarSum / numSamples)
            channel2Std = torch.sqrt(channel2VarSum / numSamples)

            Logging.log("Channel 0 std " + str(channel0Std))
            Logging.log("Channel 1 std " + str(channel1Std))
            Logging.log("Channel 2 std " + str(channel2Std))

def train(runId: str,
          runmode: str,
          resnetType: int,
          modelType: str,
          device: torch.device,
          modelStateDict: Dict = None,
          optimizerStateDict: Dict = None,
          forceLearningRate: bool = False):
    Logging.log("Initializing models for training")
    Logging.log("Using ResNet type " + str(resnetType))
    Logging.log("Learning rate: " + str(LROperations.getLearningRate(modelType)))

    datasetParameters = {'batch_size': Config.NN.BATCH_SIZE,
                         'shuffle': True,
                         'num_workers': Config.IO.NUM_DATALOADER_THREADS}
    dataType = None
    if runmode == CLArgs.Runmode.TRAIN:
        dataType = CustomFileIO.DataType.TRAIN
    elif runmode == CLArgs.Runmode.TOY:
        dataType = CustomFileIO.DataType.TOY
    else:
        dataType = CustomFileIO.DataType.TINY

    trainingLoader = DataLoader(Dataset(dataType, torch.device(CLArgs.Device.CPU)), **datasetParameters)
    validationLoader = DataLoader(Dataset(CustomFileIO.DataType.VAL, torch.device(CLArgs.Device.CPU)), ** datasetParameters)

    model, optimizer, modifiedResnet = ModelLoader.loadModelOptimizerAndResnet(modelType,
                                                               LROperations.getLearningRate(modelType),
                                                               Config.NN.REGULARIZATION,
                                                               Dataset.NUM_CLASSES,
                                                               Dataset.IGNORE_INDEX,
                                                               resnetType,
                                                               device,
                                                               modelStateDict=modelStateDict,
                                                               optimizerStateDict=optimizerStateDict)
    if forceLearningRate:
        Logging.log("Forcing learning rate to start at " + str(LROperations.getLearningRate(modelType)))
        LROperations.forceUpdateLearningRate(optimizer, LROperations.getLearningRate(modelType))

    # Loop over epochs
    iteration = 0
    validation = 1
    lowestValidationLoss = float('inf')
    for epoch in range(1, Config.NN.MAX_EPOCHS + 1):
        # fail safe
        model.train()
        Logging.log("Commencing epoch " + str(epoch))
        mostRecentLosses = deque(maxlen=Config.NN.NUM_ITERATIONS_FOR_AVERAGE)
        totalBatches = 0
        for inputs, labels in trainingLoader:
            iteration += 1
            # Logging.log("Iteration " + str(iteration))
            # Transfer to GPU
            inputs = inputs.to(device)
            labels = labels.to(device)

            inputs = DataPreparation.standardizeAndPrepareInputs(inputs, modifiedResnet)
            # print("Inputs: " + str(inputs.size()))
            # print("Labels: " + str(labels.size()))
            showPrediction = iteration % Config.NN.SHOW_FREQUENCY == 0 and str(device) == CLArgs.Device.CPU
            # if showPrediction:
                # model.eval()

            loss = model(inputs, labels, showPrediction)
            model.train()
            # Logging.log("Loss: " + str(loss))

            optimizer.zero_grad()
            loss.mean().backward()

            # here want to sum loss so we can divide it by the number of samples. Only here!
            # totalLossFromAveragedBatches += loss.mean().item()
            mostRecentLosses.append(loss.mean().item())
            totalBatches += 1
            # Logging.log("Sum: " + str(loss.sum().item()))
            # Logging.log("Mean: " + str(loss.mean().item()))
            optimizer.step()
            LROperations.annealLearningRateIfApplicable(optimizer, modelType, iteration)

            if iteration % Config.NN.LOG_FREQUENCY == 0:
                averageLoss = 0
                for value in mostRecentLosses:
                    averageLoss += value
                averageLoss = averageLoss / len(mostRecentLosses)
                Logging.log("Epoch " + str(epoch) + ", iteration " + str(iteration) + " Average loss: " + str(averageLoss))
            if iteration > 0 and iteration % Config.NN.SAVE_FREQUENCY == 0:
                Logging.log("Saving checkpoint of model")
                CustomFileIO.saveModelStateDict(model.state_dict(), runmode, str(resnetType), runId + "iter_" + str(iteration))
                CustomFileIO.saveOptimizerStateDict(optimizer.state_dict(), runmode, str(resnetType), runId + "iter_" + str(iteration))

            lowestValidationLoss, validation = ValidationOperations.runValidationIfApplicable(iteration,
                                                                                                 Config.NN.VALIDATION_FREQUENCY,
                                                                                                 modifiedResnet,
                                                                                                 validationLoader,
                                                                                                 model,
                                                                                                 optimizer,
                                                                                                 device,
                                                                                                 validation,
                                                                                                 lowestValidationLoss,
                                                                                                 runmode,
                                                                                                 resnetType,
                                                                                                 runId)

        Logging.log("End of epoch " + str(epoch))

        lowestValidationLoss, validation = ValidationOperations.runValidation(modifiedResnet,
                                                                                 validationLoader,
                                                                                 model,
                                                                                 optimizer,
                                                                                 device,
                                                                                 validation,
                                                                                 lowestValidationLoss,
                                                                                 runmode,
                                                                                 resnetType,
                                                                                 runId)

    Logging.log("Reached maximum number of epochs. Ceasing training.")
    Logging.log("Saving checkpoint of model")
    CustomFileIO.saveModelStateDict(model.state_dict(), runmode, str(resnetType), runId + "final")
    CustomFileIO.saveOptimizerStateDict(optimizer.state_dict(), runmode, str(resnetType), runId + "final")

    Logging.log("Finished training. Check logs at " + str(Logging.LOG_FILE))


def getModelSavePath(modelType: str, resnetType: int) -> str:
    path = ""
    if CLArgs.ModelType.UNET == modelType:
        if ModifiedResnet.ResnetType.SMALL_34 == resnetType:
            path = "train_res34_2019_05_28_15_55_12best"
        else:
            path = "train_res101_2019_06_01_22_55_01best"
    else:
        path = "train_res34_2019_05_29_15_01_25best"

    return path

def main(runId: str):
    Logging.LOG_FILE = Logging.generateLogFilePath(runId)
    try:
        args = docopt(__doc__)
        clArgs = CLArgs(args)
        device = clArgs.getDevice()
        runmode = clArgs.getRunmode()
        modelType = clArgs.getModelType()
        resnetType = clArgs.getResnetType()
        Logging.log("Using device " + str(device))
        Logging.log("Model type: " + str(modelType))
        Logging.log("GPUs detected: " + str(torch.cuda.device_count()))
        modelStateDict = None
        optimizerStateDict = None
        forceNewLearningRate = False
        modelSavePath = getModelSavePath(modelType, resnetType)

        assert(torch.__version__ == "1.0.0"), "Please update your installation of PyTorch. You have {} and you should have version 1.0.0".format(torch.__version__)

        Logging.log("Runmode: " + str(runmode))
        if CLArgs.Runmode.TEST == runmode:
            TestOperations.test(modelType, modelSavePath, [CustomFileIO.DataType.VAL, CustomFileIO.DataType.TEST], resnetType, device)
        elif CLArgs.Runmode.TEST_ONE_IMAGE == runmode:
            TestOperations.testAndShowOneImage(modelType, modelSavePath, "000000000285", resnetType, device)
        elif CLArgs.Runmode.CALCULATE_MEANS == runmode:
            calculateMeanAndStd(runId, runmode, device)
        elif CLArgs.Runmode.VALIDATE == runmode:
            ValidationOperations.loadModelAndRunValidation(modelType, resnetType, device, modelSavePath)
        else:
            if clArgs.isResumeRun():
                if len(modelSavePath) > 0:
                    Logging.log("Resuming run from checkpoint")
                    modelStateDict = CustomFileIO.loadModelStateDict(modelSavePath, deviceType=str(device))
                    optimizerStateDict = CustomFileIO.loadOptimizerStateDict(modelSavePath, deviceType=str(device))
                    if clArgs.shouldForceNewLearningRate():
                        forceNewLearningRate = True
                else:
                    raise ValueError("Resume run must have a valid model path")
            train(runId, runmode, resnetType, modelType, device, modelStateDict=modelStateDict, optimizerStateDict=optimizerStateDict, forceLearningRate=forceNewLearningRate)
    except Exception as e:
        Logging.log("Run failed due to exception, please check logs")
        Logging.log(e)
        raise e

if __name__ == '__main__':
    timestamp = time.time()
    runId = datetime.datetime.fromtimestamp(timestamp).strftime('%Y_%m_%d_%H_%M_%S')
    main(runId)




