from typing import *

from customlogging import Logging
import torch
import torch.nn as nn
from torch.utils.data import DataLoader

from clargs import CLArgs
from config import Config
from fileio.datasets import Dataset
from fileio.CustomFileIO import CustomFileIO
from models.modelLoader import ModelLoader
from learningRateOperations import LROperations
from davidtvs.iou import IoU
from dataPreparation import DataPreparation

from PIL import Image, ImageOps


class TestOperations:

    @staticmethod
    def test(modelType: str, modelSavePath: str, dataTypes: List[str], resnetType: int, device: torch.device) -> None:
        Logging.log("Initializing models for testing.")
        model, optimizer, modifiedResnet = ModelLoader.loadModelOptimizerAndResnet(modelType,
                                                                                   LROperations.getLearningRate(modelType),
                                                                                   Config.NN.REGULARIZATION,
                                                                                   Dataset.NUM_CLASSES,
                                                                                   Dataset.IGNORE_INDEX,
                                                                                   resnetType,
                                                                                   device)
        model.load_state_dict(CustomFileIO.loadModelStateDict(modelSavePath, deviceType=str(device)))
        model.eval()

        datasetParameters = {'batch_size': Config.NN.TESTING_BATCH_SIZE,
                             'shuffle': False,
                             'num_workers': Config.IO.NUM_DATALOADER_THREADS}

        for dataType in dataTypes:
            TestOperations.__runTest(model, modifiedResnet, dataType, datasetParameters, device)


    @staticmethod
    def __runTest(model: nn.Module, modifiedResnet: nn.Module, dataType: str, datasetParameters: Dict, device: torch.device) -> None:
        dataLoader = DataLoader(Dataset(dataType, torch.device(CLArgs.Device.CPU)), ** datasetParameters)

        iteration = 0
        totalSamples = 0
        iou = IoU(Dataset.NUM_CLASSES, ignore_index=Dataset.IGNORE_INDEX)
        Logging.log("Commencing evaluation for data type : " + dataType)
        for inputs, labels in dataLoader:
            inputs = inputs.to(device)
            labels = labels.to(device)
            totalSamples += inputs.shape[0]
            iteration += 1

            inputs = DataPreparation.standardizeAndPrepareInputs(inputs, modifiedResnet)
            predictions = model.module.predict(inputs)
            if len(labels.shape) < 3:
                labels = labels.unsqueeze(0)
            if len(predictions.shape) < 3:
                predictions = predictions.unsqueeze(0)
            iou.add(predictions, labels)

            if iteration % 1000 == 0:
                Logging.log("Completed " + str(iteration) + " iterations. Total samples: " + str(totalSamples) + ", Mean IOU: " + str(iou.value()[1]))

        Logging.log("Finished all " + str(iteration) + " iterations. Total samples: " + str(totalSamples) + ", Mean IOU: " + str(iou.value()[1]))

    @staticmethod
    def testAndShowOneImage(modelType: str, modelSavePath: str, imageId: str, resnetType: int, device: torch.device) -> None:
        Logging.log("Initializing models for testing single image.")
        model, optimizer, modifiedResnet = ModelLoader.loadModelOptimizerAndResnet(modelType,
                                                                                   LROperations.getLearningRate(modelType),
                                                                                   Config.NN.REGULARIZATION,
                                                                                   Dataset.NUM_CLASSES,
                                                                                   Dataset.IGNORE_INDEX,
                                                                                   resnetType,
                                                                                   device,
                                                                                   modelStateDict=CustomFileIO.loadModelStateDict(modelSavePath, deviceType=str(device)))
        model.eval()

        TestOperations.showPredictionForInput(modifiedResnet, model, imageId, device)

    @staticmethod
    def showPredictionForInput(modifiedResnet: nn.Module, model: nn.Module, imageId: str, device: torch.device) -> None:
        input = CustomFileIO.loadInputImage(CustomFileIO.DataType.TOY, str(imageId), Dataset.RESNET_INPUT_SIZE, device)
        input = input.unsqueeze(0)
        input = DataPreparation.standardizeAndPrepareInputs(input, modifiedResnet)
        predictionTensor = model.module.predict(input)
        nonZeroMask = (predictionTensor > 0).long()
        predictionTensor = (predictionTensor + 91) * nonZeroMask
        npImage = predictionTensor.numpy()

        labelImage = CustomFileIO.loadAndResizePilImage(CustomFileIO.DataType.TOY,
                                                        str(imageId),
                                                        CustomFileIO.ImageType.LABEL,
                                                        Dataset.RESNET_INPUT_SIZE,
                                                        ".png",
                                                        False)
        predictionImage = Image.fromarray(npImage.astype('uint8'), 'P')
        predictionImage = ImageOps.expand(predictionImage, 0)
        predictionImage.putpalette(labelImage.palette.palette)
        predictionImage.save("prediction.png")
        labelImage.show()
        predictionImage.show()