from typing import *

from customlogging import Logging
import torch
import torch.nn as nn
from torch.utils.data import DataLoader

from fileio.CustomFileIO import CustomFileIO
from dataPreparation import DataPreparation
from fileio.datasets import Dataset
from models.modelLoader import ModelLoader
from clargs import CLArgs
from config import Config
from learningRateOperations import LROperations


class ValidationOperations:

    @staticmethod
    def runValidationIfApplicable(iteration: int,
                                  validationFreqency: int,
                                  modifiedResnet: nn.Module,
                                  validationLoader: DataLoader,
                                  model: nn.Module,
                                  optimizer: torch.optim.Optimizer,
                                  device: torch.device,
                                  validation: int,
                                  lowestValidationLoss: float,
                                  runmode: str,
                                  resnetType: int,
                                  runId: str) -> Tuple[float, int]:
        if iteration % validationFreqency == 0:
            return ValidationOperations.runValidation(modifiedResnet, validationLoader, model, optimizer, device, validation, lowestValidationLoss, runmode, resnetType, runId)
        else:
            return lowestValidationLoss, validation

    @staticmethod
    def runValidation(modifiedResnet: nn.Module,
                      validationLoader: DataLoader,
                      model: nn.Module,
                      optimizer: torch.optim.Optimizer,
                      device: torch.device,
                      validation: int,
                      lowestValidationLoss: float,
                      runmode: str,
                      resnetType: int,
                      runId: str) -> Tuple[float, int]:
        validationLoss = ValidationOperations.__validate(modifiedResnet, validationLoader, model, device, validation)
        validation += 1

        if validationLoss < lowestValidationLoss:
            Logging.log("Validation loss lower than previous best (" + str(lowestValidationLoss) + "). Saving model.")
            CustomFileIO.saveModelStateDict(model.state_dict(), runmode, str(resnetType), runId + "best")
            CustomFileIO.saveOptimizerStateDict(optimizer.state_dict(), runmode, str(resnetType), runId + "best")

            lowestValidationLoss = validationLoss
        else:
            Logging.log("Validation loss higher than previous best (" + str(lowestValidationLoss) + "), not saving")

        return lowestValidationLoss, validation

    @staticmethod
    def loadModelAndRunValidation(modelType: str, resnetType: int, device: torch.device, path: str) -> None:
        datasetParameters = {'batch_size': Config.NN.BATCH_SIZE,
                             'shuffle': True,
                             'num_workers': Config.IO.NUM_DATALOADER_THREADS}
        validationLoader = DataLoader(Dataset(CustomFileIO.DataType.VAL, torch.device(CLArgs.Device.CPU)), ** datasetParameters)
        modelStateDict = CustomFileIO.loadModelStateDict(path, deviceType=str(device))
        model, optimizer, modifiedResnet = ModelLoader.loadModelOptimizerAndResnet(modelType,
                                                                                   LROperations.getLearningRate(modelType),
                                                                                   Config.NN.REGULARIZATION,
                                                                                   Dataset.NUM_CLASSES,
                                                                                   Dataset.IGNORE_INDEX,
                                                                                   resnetType,
                                                                                   device,
                                                                                   modelStateDict=modelStateDict,
                                                                                   optimizerStateDict=None)
        ValidationOperations.__validate(modifiedResnet, validationLoader, model, device, 0)

    @staticmethod
    def __validate(modifiedResnet: nn.Module, validationLoader: DataLoader, model: nn.Module, device: torch.device, validationId: int) -> float:
        Logging.log("Beginning validation cycle " + str(validationId))
        # SUPER IMPORTANT
        model.eval()
        totalAverageLoss = 0
        totalSamples = 0
        numBatches = 0
        with torch.set_grad_enabled(False):
            for inputs, labels in validationLoader:
                # Transfer to GPU
                inputs = inputs.to(device)
                labels = labels.to(device)

                totalSamples += inputs.shape[0]
                inputs = DataPreparation.standardizeAndPrepareInputs(inputs, modifiedResnet)
                loss = model(inputs, labels)
                totalAverageLoss += loss.mean().item()
                # Logging.log("Sum: " + loss.sum().item())
                # Logging.log("Mean: " + loss.mean().item())
                numBatches += 1

        averageLoss = totalAverageLoss / numBatches
        Logging.log("Validation " + str(validationId) + " -- Samples processed: " + str(totalSamples) + ", Total loss: " + str(totalAverageLoss) + ", Average loss: " + str(averageLoss))

        # SUPER IMPORTANT
        model.train()
        return averageLoss
